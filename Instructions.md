```
0000 0000 |           | nop
0000 0001 |           | rst
0000 0010 |           | hlt
0000 0011 |   Value   | int

0000 0100
0000 0101
0000 0110
0000 0111
0000 1000
0000 1001
0000 1010

0000 1011 |           | carry
0000 1100 |           | sign

0000 1101 |           | rtn
0000 1110 |   Addr    | call
0000 1111 | 0Src 0000 | callra

0001 0000 |           | pop
0001 0001 |   Addr    | pusha
0001 0010 |   Value   | pushi
0001 0011 |   Addr    | popa

0001 0100 |   Addr    | jabs
0001 0101 | 0Src 0000 | jabsra
0001 0110 |   Addr    | jrel
0001 0111 | 0Src 0000 | jrelra

0001 1Flg |   Addr    | jabs flag
0010 0Flg | 0Src 0000 | jabsra flag
0010 1Flg |   Addr    | jrel flag
0011 0Flg | 0Src 0000 | jrelra flag

0011 1Dst | 0Src 0000 | ldra
0100 0Src | 0Src 0000 | stora
0100 1Dst |   Value   | ldi
0101 0Dst |   Addr    | lda
0101 1Src |   Addr    | sto

0110 0Src |           | pushr
0110 1Src |           | pushra
0111 0Dst |           | popr
0111 1Dst |           | popra

1000 0Dst | 0Src 0Src | add
1000 1Dst | 0Src 0Src | sub
1001 0Dst | 0Src 0Src | and
1001 1Dst | 0Src 0Src | or
1010 0Dst | 0Src 0Src | xor
1010 1Dst | 0Src 0Src | idx
1011 0Src | 0Src 0000 | cmp
1011 1Src |   Addr    | cmpa
1100 0Dst |   Value   | addi
1100 1Dst |   Value   | subi
1101 0Dst |   Value   | andi
1101 1Dst |   Value   | ori
1110 0Dst |   Value   | xori
1110 1Dst |   Value   | idxi
1111 0Src |   Value   | cmpi
1111 1Src | 0Src 0000 | cmpra
```

```
int     - Raise an interrupt
carry   - Use the carry flag from the last operation
sign    - Treat values as two's compliment

ldi     - Load register from immediate
lda     - Load register from address
ldra    - Load register from register address
sto     - Store register to address
stora   - Store register to register address

jabs    - Jump absolute
jabsra  - Jump absolute to register address
jabs flag   - Jump absolute if flag
jabsra flag - Jump absolute to register address if flag

pushi   - Push immediate to stack
pusha   - Push value to stack from address
pushr   - Push value to stack from register
pushra  - Push value to stack from register address
pop     - Pop value from stack to nothingness
popa    - Pop value from stack to address
popr    - Pop value from stack to register
popra   - Pop value from stack to register address

nop     - No operation
rst     - Reset
hlt     - Halt the machine

add     - Add
sub     - Subtract
and     - Bitwise AND
or      - Bitwise OR
xor     - Bitwise XOR
idx     - Bit index

addi    - All of these same as above but in-place with immediate source instead of registers
subi
andi
ori
xori
idxi

cmp     - Compare (Point A and B to two registers and generate comparison flags)
cmpi    - Same as cmp but use the immediate value instead of the second register
cmpa    - Same as cmpi but use the immediate as a memory address
cmpra   - Same as cmpa but use the immediate as a register index from which to fetch the memory address
```

# Registers

0. GPR A
1. GPR B
2. GPR C
3. GPR D
4. LFSR
5. Page
6. Data Stack Ptr
7. Call Stack Ptr

# Flags

0. C
1. EQ
2. GT
3. LT

Instruction decoding can also extract 4 additional conditions: NC (no carry), NE, LE, and GE by inverting the flags.

# Instruction decoding steps

- ldi
    1. Addr=PC, MemOut, InstrIn
    2. PCInc
    3. Addr=PC, Reg=Instr, MemOut, RegIn
    4. PCInc, SCRes
- sto
    1. Addr=PC, MemOut, InstrIn
    2. PCInc
    3. Addr=PC, MemOut, ArgIn
    4. Addr=Arg, Reg=Instr, RegOut, MemIn
    5. PCInc, SCRes
- add
    1. Addr=PC, MemOut, InstrIn
    2. PCInc
    3. Addr=PC, MemOut, ArgIn
    4. ALUIn=Arg, ALUSelA, Reg=Instr, ALUOut, RegIn
    5. PCInc, SCRes
- addi
    1. Addr=PC, MemOut, InstrIn
    2. PCInc
    3. Addr=PC, MemOut, ArgIn
    3. ALUIn=Instr, ALUOvrB, Reg=Instr, ALUOut, RegIn
    5. PCInc, SCRes
