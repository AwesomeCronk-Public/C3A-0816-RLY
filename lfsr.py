import sys

value = int(sys.argv[1])
if value > 255:
    print('Limiting to 8 bits via bitwise AND')
    value = value & 255
    print(value)
print(bin(value)[2:].zfill(8), value)

def lfsr(oval):
    obits = []
    for i in range(8):
        bit = (oval & (2 ** i)) >> i
        obits.append(bit)
    
    nbits = []
    nbits.append(obits[7])
    nbits.append(obits[0])
    nbits.append(obits[1] ^ obits[7])
    nbits.append(obits[2] ^ obits[7])
    nbits.append(obits[3] ^ obits[7])
    nbits.append(obits[4])
    nbits.append(obits[5])
    nbits.append(obits[6])

    nval = 0
    nbits.reverse()
    for i in range(8):
        nval <<= 1
        nval += nbits[i]
    
    return nval

for i in range(int(sys.argv[2])):
    value = lfsr(value)
    print(bin(value)[2:].zfill(8), value)
